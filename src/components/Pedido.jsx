import React, { useEffect } from "react";

const Pedido = ({ orden, cancelarOrden }) => {
  useEffect(() => {
    console.log("Se actualizó el componente");
  }, [orden]);

  return (
    <div>
      {orden ? (
        <div>
          <p>Tu pedido: {orden}</p>
          <button onClick={cancelarOrden}>Cancelar Pedido</button>
        </div>
      ) : (
        <p>Cargando pedido...</p>
      )}
    </div>
  );
};

export default Pedido;
