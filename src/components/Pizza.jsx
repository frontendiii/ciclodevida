import React, { useState, useEffect } from "react";

const Pizza = ({ setOrden }) => {
  const [cargando, setCargando] = useState(true);

  useEffect(() => {
    const timer = setTimeout(() => {
      setOrden("Pizza de peperoni");
      setCargando(false);
    }, 2000);
    return () => {
      clearTimeout(timer);
    };
  }, [setOrden]);

  return (
    <div>
      <h2>Realizar Pedido</h2>
      {cargando ? (
        <p>Cargando pedido... </p>
      ) : (
        <p>Tu pedido ha sido realizado.</p>
      )}
    </div>
  );
};

export default Pizza;
