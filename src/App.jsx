import React, { useState } from "react";
import "./App.css";
import Pizza from "./components/Pizza";
import Pedido from "./components/Pedido";

function App() {
  const [orden, setOrden] = useState(null);
  const cancelarOrden = () => {
    setOrden(null);
    console.log("El componente fue desmontado")
  };
  return (
    <React.Fragment>
      <Pizza setOrden={setOrden}/>
      <Pedido orden={orden} cancelarOrden={cancelarOrden}/>
    </React.Fragment>
  );
}

export default App;
